# Autor

David Cantón Nadales

## Angular version

10.2.4

## Tests

Jasmine:
TOTAL: 22 SUCCESS
TOTAL: 22 SUCCESS
TOTAL: 22 SUCCESS

=============================== Coverage summary ===============================
Statements : 86.47% ( 115/133 )
Branches : 72.73% ( 16/22 )
Functions : 80.85% ( 38/47 )
Lines : 85.59% ( 101/118 )

e2e:
workspace-project App
✓ Should Show header
✓ Input 100 in bet
✓ Click OK Bet button
[10:42:46] W/element - more than one element found for locator By(css selector, _[id="ball1"]) - the first result will be used
✓ Click in a ball
[10:42:46] W/element - more than one element found for locator By(css selector, _[id="ball1"]) - the first result will be used
✓ Click start game
[10:42:47] W/element - more than one element found for locator By(css selector, \*[id="ball1"]) - the first result will be used
✓ Check result

Executed 6 of 6 specs SUCCESS in 10 secs.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Contact

davidcantonnadales@gmail.com
