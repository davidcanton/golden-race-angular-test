import { Component, OnInit } from '@angular/core';
import { Ball } from '../../class/ball/ball';
import { Settings } from '../../class/settings/settings';
import { Observable, Subject } from 'rxjs';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-ball-selector',
  templateUrl: './ball-selector.component.html',
  styleUrls: ['./ball-selector.component.css'],
})
export class BallSelectorComponent implements OnInit {
  settings = new Settings();
  balls: Ball[] = [];
  loading: boolean;

  constructor(private storeService: StoreService) {
    this.storeService.getBallsSubscribe().subscribe((value) => {
      this.balls = value;
    });
  }

  ngOnInit(): void {
    this.loading = true;
    //GENERATE BALLS

    this.storeService.createBalls();

    this.loading = false;
  }

  clearSelection() {
    this.storeService.clearSelection();
  }
}
