import { Component, OnInit, Input } from '@angular/core';
import { Ball } from '../../class/ball/ball';
import { StoreService } from '../../services/store.service';
import { MessagingService } from '../../services/messaging.service';

@Component({
  selector: 'app-ball',
  templateUrl: './ball.component.html',
  styleUrls: ['./ball.component.css'],
})
export class BallComponent implements OnInit {
  @Input() ball: Ball = new Ball();
  @Input() isSelected: boolean;

  constructor(
    private storeService: StoreService,
    private messagingService: MessagingService
  ) {}

  selectBall(): any {
    var result = this.storeService.selectOneBall(this.ball);

    if (result.status) {
      this.messagingService.showNotification(result.message);
    } else {
      this.messagingService.showError(result.message);
    }
  }

  ngOnInit(): void {}
}
