import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-game-panel',
  templateUrl: './game-panel.component.html',
  styleUrls: ['./game-panel.component.css'],
})
export class GamePanelComponent implements OnInit {
  isPlaying: boolean;

  constructor(private storeService: StoreService) {
    this.storeService.getIsPlaying().subscribe((value) => {
      this.isPlaying = value;
    });
  }

  ngOnInit(): void {}
}
