import { Component, OnInit } from '@angular/core';
import { Ball } from '../../class/ball/ball';
import { StoreService } from '../../services/store.service';
import { Settings } from '../../class/settings/settings';

@Component({
  selector: 'app-game-result',
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.css'],
})
export class GameResultComponent implements OnInit {
  settings = new Settings();
  winningBall: Ball = new Ball(0, '#fff');
  isWinner: boolean;
  earning: number;
  bet: number;

  constructor(private storeService: StoreService) {}

  ngOnInit(): void {
    this.storeService.getBet().subscribe((value) => {
      this.bet = value;
    });

    this.checkResult();
  }

  checkResult() {
    this.winningBall = this.storeService.getWinningBall();

    this.isWinner = this.storeService.checkIsWinner(this.winningBall?.number);
    debugger;
    if (this.isWinner) {
      this.earning = this.bet * this.settings.profit;
    }

    setTimeout(() => {
      this.storeService.resetGame();
    }, 5000);
  }
}
