import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Ball } from '../../class/ball/ball';
import { StoreService } from '../../services/store.service';
import { MessagingService } from '../../services/messaging.service';
import { Settings } from '../../class/settings/settings';

@Component({
  selector: 'app-bet-slip',
  templateUrl: './bet-slip.component.html',
  styleUrls: ['./bet-slip.component.css'],
})
export class BetSlipComponent implements OnInit {
  settings = new Settings();
  selectedBalls: Ball[] = [];
  betForm = new FormGroup({
    bet: new FormControl(0),
  });

  bet: number = 0;

  constructor(
    private storeService: StoreService,
    private messagingService: MessagingService
  ) {}

  ngOnInit(): void {
    this.storeService.getBallsSubscribe().subscribe((value) => {
      this.selectedBalls = value.filter((item) => item.isSelected);
      console.log(this.selectedBalls);
    });
  }

  onSubmit() {
    this.bet = this.betForm.value.bet * this.settings.minimumBet;

    if (this.bet < this.settings.minimumBet) {
      this.messagingService.showNotification(
        'Minimum bet is 5 euro, try again'
      );
      return;
    }
    this.storeService.setBet(this.bet);
  }

  startGame() {
    this.storeService.setIsPlaying(true);
  }
}
