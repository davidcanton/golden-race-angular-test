export class Settings {
  minimumBet: number;
  profit: number;
  ballsInDrum: number;
  ballsColors: string[] = [];

  constructor() {
    this.minimumBet = 5;
    this.profit = 1.5;
    this.ballsInDrum = 10;

    this.ballsColors = [
      '#d55353',
      '#fff8e5',
      '#45a86a',
      '#fcc83d',
      '#ebf5ed',
      '#d95052',
      '#fff9e3',
      '#48a769',
      '#fceceb',
    ];
  }
}
