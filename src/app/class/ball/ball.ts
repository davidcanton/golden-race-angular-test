export class Ball {
  number: number = 0;
  color: string = '#fff';
  isSelected: boolean = false;

  constructor(number?, color?) {
    this.number = number || 0;
    this.color = color || '#fff';
    this.isSelected = false;
  }
}
