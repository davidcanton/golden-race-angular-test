import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { BallComponent } from '../components/ball/ball.component';
import { Ball } from '../class/ball/ball';
import { StoreService } from './store.service';

describe('StoreService', () => {
  let service: StoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [MatSnackBarModule] });
    service = TestBed.inject(StoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should show Ball is null if no parameter input', () => {
    var result = service.selectOneBall(null);

    //mat-simple-snackbar
    expect(result.message).toContain('Ball is null');
  });

  it('should show OUCH¡ You need place a bet first¡ because need bet first', () => {
    var ball = new Ball();

    var result = service.selectOneBall(ball);

    //mat-simple-snackbar
    expect(result.message).toContain('OUCH¡ You need place a bet first¡');
  });

  it('should show OUCH¡ You only can select one ball per game because i select two balls before play', () => {
    service.createBalls();

    var ball1 = new Ball(1);
    ball1.isSelected = true;
    var ball2 = new Ball(2);
    ball2.isSelected = true;

    service.setBet(100);

    service.selectOneBall(ball1);
    var result = service.selectOneBall(ball2);

    //mat-simple-snackbar
    expect(result.message).toContain(
      'OUCH¡ You only can select one ball per game'
    );
  });

  it('should contains good luck because the selection was succesful', () => {
    service.createBalls();

    var ball1 = new Ball(1);
    ball1.isSelected = true;

    service.setBet(100);

    var result = service.selectOneBall(ball1);

    //mat-simple-snackbar
    expect(result.message).toContain('good luck');
  });

  it('should create 10 balls', () => {
    service.createBalls();

    var balls = service.getBalls();
    debugger;
    //mat-simple-snackbar
    expect(balls.length).toEqual(10);
  });

  it('should deselect all the balls', () => {
    service.createBalls();

    var ball1 = new Ball(1);
    ball1.isSelected = true;
    service.setBet(100);

    var result = service.selectOneBall(ball1);

    service.clearSelection();

    //mat-simple-snackbar
    expect(
      service.getBalls().filter((item) => item.isSelected === true).length
    ).toEqual(0);
  });

  it('should set bet to 100', async () => {
    service.setBet(100);

    var bet = service.getActualBet();
    //mat-simple-snackbar
    expect(bet).toEqual(100);
  });

  it('should set is playing true', async () => {
    service.setIsPlaying(true);

    var value = service.getActualIsPlaying();
    //mat-simple-snackbar
    expect(value).toEqual(true);
  });

  it('should win', async () => {
    service.createBalls();

    var winningBall = service.getWinningBall();

    var ball1 = new Ball(winningBall.number);
    ball1.isSelected = true;

    service.setBet(100);

    var result = service.selectOneBall(ball1);

    var isWinner = service.checkIsWinner(ball1.number);

    expect(isWinner).toEqual(true);
  });

  it('should loose', async () => {
    service.createBalls();

    var winningBall = service.getWinningBall();

    var ball1 = new Ball(10);
    ball1.isSelected = true;

    service.setBet(100);

    var result = service.selectOneBall(ball1);

    var isWinner = service.checkIsWinner(ball1.number);

    expect(isWinner).toEqual(true);
  });
});
