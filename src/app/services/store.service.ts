import { Injectable } from '@angular/core';

import { Ball } from '../class/ball/ball';
import { MessagingService } from '../services/messaging.service';
import { Settings } from '../class/settings/settings';

import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  private settings = new Settings();

  private balls = new BehaviorSubject([]);

  private winningBall = new BehaviorSubject(Ball);

  private bet = new BehaviorSubject(0);

  private isPlaying = new BehaviorSubject(false);

  constructor(public messagingService: MessagingService) {}

  public createBalls() {
    var _balls = [];

    for (var i = 0; i < this.settings.ballsInDrum; i++) {
      _balls[i] = new Ball(i + 1, this.settings.ballsColors[i]);
    }

    this.balls.next(_balls);
  }

  public getBallsSubscribe(): Observable<any> {
    return this.balls.asObservable();
  }

  public selectOneBall(ball: Ball) {
    //CHECK BET
    if (!ball) {
      return { status: false, message: `Ball is null` };
    }

    if (this.bet.value < this.settings.minimumBet) {
      return { status: false, message: 'OUCH¡ You need place a bet first¡' };
    }

    //CHECK IF IS ALREADY CHECKED

    var exist =
      this.balls.getValue().filter((item) => item.isSelected).length > 0;

    if (exist) {
      return {
        status: false,
        message: `OUCH¡ You only can select one ball per game`,
      };
    } else {
      var _balls = this.balls.getValue();

      var index = _balls.findIndex((item) => item.number === ball.number);

      _balls[index].isSelected = true;

      this.balls.next(_balls);

      return {
        status: true,
        message: `Ball ${ball.number} selected, good luck¡`,
      };
    }
  }

  public clearSelection() {
    var _balls = this.balls.getValue();

    _balls.map((item) => (item.isSelected = false));

    this.balls.next(_balls);
  }

  public getBet(): Observable<any> {
    return this.bet.asObservable();
  }

  public getActualBet(): number {
    return this.bet.value;
  }

  public setBet(quantity: number) {
    this.bet.next(quantity);
  }

  public getIsPlaying(): Observable<any> {
    return this.isPlaying.asObservable();
  }

  public getActualIsPlaying(): boolean {
    return this.isPlaying.value;
  }

  public setIsPlaying(status: boolean) {
    this.isPlaying.next(status);
  }

  public getWinningBall() {
    var min = Math.ceil(0);
    var max = Math.floor(9);

    var random = Math.floor(Math.random() * (max - min + 1)) + min;

    return this.balls.value[random];
  }

  public checkIsWinner(ballNumber: number): boolean {
    var _balls = this.balls.value.filter((item) => item.isSelected);

    if (_balls.length === 0) {
      return false;
    } else {
      return ballNumber === _balls[0].number;
    }
  }

  public resetGame() {
    this.isPlaying.next(false);
    this.clearSelection();
  }

  public getBalls() {
    return this.balls.value;
  }
}
