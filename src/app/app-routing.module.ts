import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GamePanelComponent } from './components/game-panel/game-panel.component';

const routes: Routes = [
  { path: '', redirectTo: '/game-panel', pathMatch: 'full' },

  { path: 'game-panel', component: GamePanelComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
