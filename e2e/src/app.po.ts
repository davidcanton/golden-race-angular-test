import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getHeadingText() {
    // Get the home page heading element reference
    return element(by.id('header')).getText();
  }

  getTitleText(): Promise<string> {
    return element(
      by.css('app-root .content span')
    ).getText() as Promise<string>;
  }

  async setBet() {
    await element(by.id('bet-input')).clear();
    await element(by.id('bet-input')).sendKeys('100');

    return element(by.id('bet-input')).getAttribute('value') as Promise<string>;
  }

  async clickBetButton() {
    await element(by.id('bet-form')).submit();

    var EC = browser.ExpectedConditions;
    var elm = element(by.id('bet-total'));
    browser.wait(EC.textToBePresentInElement(elm, 'Total: 500 €'), 5000);

    const value = await element(by.id('bet-total')).getText();

    return value;
  }

  async selectBall() {
    await element(by.id('ball1')).click();

    var style = await element(by.id('ball1')).getAttribute('disabled');

    return style;
  }

  async clickStartButton() {
    await element(by.id('btn-start-game')).click();
    return true;
  }

  async checkResult() {
    var result = await element(by.id('game-panel')).isPresent();

    return result;
  }

  getBet(): Promise<string> {
    return element(by.css('.input-style')).getText() as Promise<string>;
  }
}
