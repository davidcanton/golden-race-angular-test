import { AppPage } from './app.po';
import { browser, logging, by } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Should Show header', function () {
    page.navigateTo();
    expect(page.getHeadingText()).toEqual('Golden Race Angular Test');
  });

  it('Input 100 in bet', async () => {
    await page.navigateTo();

    var value = await page.setBet();

    expect(value).toEqual('100');
  });

  it('Click OK Bet button', async () => {
    await page.navigateTo();

    await page.setBet();
    var value = await page.clickBetButton();

    expect(value).toEqual('Total: 500 €');
  });

  it('Click in a ball', async () => {
    await page.navigateTo();

    await page.setBet();
    await page.clickBetButton();

    var result = await page.selectBall();

    expect(result).toEqual('true');
  });

  it('Click start game', async () => {
    await page.navigateTo();

    await page.setBet();
    await page.clickBetButton();

    await page.selectBall();

    var result = await page.clickStartButton();

    expect(result).toBe(true);
  });

  it('Check result', async () => {
    await page.navigateTo();

    await page.setBet();
    await page.clickBetButton();

    await page.selectBall();

    await page.clickStartButton();

    var result = await page.checkResult();

    expect(result).toBe(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
